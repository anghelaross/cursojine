package gob.ine.cognos.utilitario;

public class Calculadora {
	public final static double VALOR_MINIMO=-100000;
	public static double calcularPromedio(double[] array) {
		double media=0;
	    for (int i=0; i < array.length; i++) {
	        media = media + array[i];
	       }
	    media = media / array.length;
		return media;
	}
	public static double obtenerMayor(double[] array) {
	    double iNumeroMayor, iPosicion;	     
	    iNumeroMayor = VALOR_MINIMO;   //array[0];
	    iPosicion = 0;
	    for (int x=1;x<array.length;x++) {
	        if (array[x]>iNumeroMayor){
	            iNumeroMayor = array[x];
	            iPosicion = x;
	         } 
	    }
	    return iNumeroMayor;
	}
}
