package gob.ine.cognos.estatic.principal;

import gob.ine.cognos.utilitario.Calculadora;

public class PrincipalCalculadora {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		double array[]= {10,25,30,8,6,9,1,15,42};
		double promedio;
		double maximo;
		promedio=Calculadora.calcularPromedio(array);
		maximo=Calculadora.obtenerMayor(array);
		
		System.out.println("Promedio= "+promedio);
		System.out.println("Maximo= "+maximo);
		System.out.println("Minimo= "+ Calculadora.VALOR_MINIMO);
	}

}
