package gob.ine.cognos.servlets;
import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


@WebServlet("/hola")
public class HolaServlet extends HttpServlet{
	
	@Override
	public void init() throws ServletException {
		System.out.println("Iniciando HolaServlet");
	}
	
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		System.out.println("Llamando get");
		String nombre = req.getParameter("nombre");
		//super.doGet(req, resp);
		PrintWriter out = resp.getWriter();
		out.println("<b>HOLA </b>"+ nombre);
		out.println("<HTML><HEAD><TITLE>");
		out.println("</TITLE></HEAD><BODY BGCOLOR=\"#CCBBAA\">");
		//out.println("<H1>" + title + "</H1>");
		out.println("<P>This is output from SimpleServlet.");
		out.println("<H1>Te llamas" + req.getParameter("nombre") + "</H1><br>" );
		out.println("</BODY></HTML>");
		out.close();
	}
	
	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		System.out.println("Llamando get");
		String nombre = req.getParameter("nombre");
		//super.doGet(req, resp);
		PrintWriter out = resp.getWriter();
		out.println("<b>HOLA </b>"+ nombre);
		out.println("<HTML><HEAD><TITLE>");
		out.println("</TITLE></HEAD><BODY BGCOLOR=\"#CFFFAA\">");
		//out.println("<H1>" + title + "</H1>");
		out.println("<H1>Prueba SimpleServlet.</H1>");
		out.println("<H2>Te llamas " + req.getParameter("nombre") + "</H2><br>" );
		out.println("</BODY></HTML>");
		out.close();
	}

}
