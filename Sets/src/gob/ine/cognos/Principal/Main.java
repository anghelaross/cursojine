package gob.ine.cognos.Principal;

//import java.util.ArrayList;
import java.util.HashSet;
//import java.util.List;
import java.util.Scanner;
import java.util.Set;

import gob.ine.cognos.Producto.Producto;


public class Main {

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		char respuesta;
		Set<Producto> productos = new HashSet<Producto>();
		// LinkedList<Producto> productos = new LinkedList<Producto>();
		do {
			System.out.println("Ingrese los datos del producto");
			System.out.println("Ingrese nombre del producto");
			String nombre = sc.nextLine();
			System.out.println("Ingrese precio del producto");
			double precio = sc.nextDouble();
			System.out.println("Ingrese cantidad del producto");
			int cantidad = sc.nextInt();

			Producto producto = new Producto(nombre, precio, cantidad);
			productos.add(producto);

			System.out.println("Desea seguir adicionando productos? S/N");
			String cad = sc.nextLine();
			cad = sc.nextLine();
			respuesta = cad.charAt(0);
		} while (respuesta == 'S');

		System.out.println("la lista de productos es");
		listarProductos(productos);

		System.out.println("Desea eliminar productos? S/N");
		String cad = sc.nextLine();
		respuesta = cad.charAt(0);

		while (respuesta == 'S') {

			System.out.println("INDIQUE EL NÚMERO DE PRODUCTO QUE DESEA ELIMINAR");
			listarProductos(productos);
			int op = sc.nextInt();
			productos.remove(op);
			System.out.println("Desea eliminar productos? S/N");
			cad = sc.nextLine();
			if (cad.length() == 0)
				cad = sc.nextLine();
			respuesta = cad.charAt(0);
		}

		listarProductos(productos);

		System.out.println("TOTAL " + precioTotal((HashSet<Producto>) productos));
	}

	private static void listarProductos(Set<Producto> productos) {
		int c = 1;
		System.out.println("Nª \t Producto \t Precio \t Cantidad");
		for (Producto producto : productos) {
			System.out.println(c + "\t" + producto.getNombre() +"\t" + producto.getPrecio() + "\t" +producto.getCantidad());
			c++;
		}
	}

	private static double precioTotal(HashSet<Producto> productos) {
		double res = 0;
		for (Producto producto : productos) {
			res = res + (producto.getPrecio() * producto.getCantidad());
		}
		return res;
	}

}
