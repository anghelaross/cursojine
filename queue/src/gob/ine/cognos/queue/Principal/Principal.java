package gob.ine.cognos.queue.Principal;

import java.util.Comparator;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.Map;
import java.util.PriorityQueue;
import java.util.Queue;

import gob.ine.cognos.queue.Objetos.Persona;

public class Principal {

	public static void main(String[] args) {
		Comparator<Persona> comparator=new Comparator<Persona>() {

			@Override
			public int compare(Persona arg0, Persona arg1) {
				
				return arg1.getNombre().length()-arg0.getNombre().length();
			}
		};
		Queue<Persona> personas =new PriorityQueue<Persona>(comparator);
		Persona p1 =new Persona("Angela");
		Persona p2 =new Persona("Juan Carlos");
		Persona p3 =new Persona("Roberto");
		Persona p4 =new Persona("Sergio");
		personas.add(p1);
		personas.add(p2);
		personas.add(p3);
		personas.add(p4);
		System.out.println(personas.size());
		while(!personas.isEmpty()) {
			System.out.println(personas.remove().getNombre());
			System.out.println(personas.size());
		}
		Map<String, Persona> personaMap = new HashMap<String, Persona>();
		personaMap.put("Angela", p1);
		personaMap.put(p2.getNombre(), p2);
		personaMap.put(p3.getNombre(), p3);
		personaMap.put(p4.getNombre(), p4);
		
		System.out.println();
		System.out.println(personaMap.get("Sergio").getNombre());
		personaMap.put(p4.getNombre(), p1);
		System.out.println(personaMap.get("Sergio").getNombre());
		if(personaMap.containsKey("Sergio"))
			System.out.println("Sergio existe");
		if(personaMap.containsKey("Ariel"))
			System.out.println("Ariel existe");
		else
			System.out.println("Ariel no existe");
 	}

}
