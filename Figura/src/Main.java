
public class Main {
	
	public static void main(String[] args) {
		Figura circulo = new Circulo(10);
		System.out.print("CIRCULO ");
		mostrarArea(circulo);
		
		Figura cuadrado = new Cuadrado(8);
		System.out.print("CUADRADO ");
		mostrarArea(cuadrado);
		
		Figura rectangulo = new Rectangulo(8, 5);
		System.out.print("RECTANGULO ");
		mostrarArea(rectangulo);
		
	}
	
	public static void mostrarArea(Figura figura) {
		double area = figura.calcularArea();
		System.out.println("El area es: " + area);
		
	}
	
}
