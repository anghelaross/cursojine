
public class Rectangulo extends Figura{
	double alto;
	double base;
	public Rectangulo() {
		
	}
	
	public Rectangulo(double alto, double base) {
		this.alto = alto;
		this.base = base;
	}

	public double getAlto() {
		return alto;
	}
	public void setAlto(double alto) {
		this.alto = alto;
	}
	public double getAncho() {
		return base;
	}
	public void setAncho(double ancho) {
		this.base = ancho;
	}
	@Override
	public double calcularArea() {
    	return base*alto;
    }
}
