package gob.ine.cognos.excepciones.objetos;

public class ValidacionException extends Exception{

	private int codError;
	public ValidacionException() {
		super();
		// TODO Auto-generated constructor stub
	}

	public ValidacionException(String arg0) {
		super(arg0);
		// TODO Auto-generated constructor stub
	}
	public ValidacionException(String arg0, int codError) {
		super(arg0);
		this.codError=codError;
	}

	public int getCodError() {
		return codError;
	}

	public void setCodError(int codError) {
		this.codError = codError;
	}

}
