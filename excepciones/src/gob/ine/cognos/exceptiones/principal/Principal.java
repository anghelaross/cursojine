package gob.ine.cognos.exceptiones.principal;

import java.util.Scanner;

import gob.ine.cognos.excepciones.objetos.ValidacionException;

public class Principal {

	public static void main(String[] args) {
		Scanner sc=new Scanner(System.in);
		String nombre;
		String apellido;
		int edad;
		int a=5;
		int b=0;
		int r;
		nombre = sc.nextLine();
		apellido= sc.nextLine();
		edad = sc.nextInt();
		
		try {
			if(validarCadena(nombre)) {
				System.out.println("Nombre Correcto");
			}
			r=a/b;
		}catch(ValidacionException e) {
			System.out.println("Error en la validación del nombre");
			System.out.println(e.getMessage());
			System.out.println(e.getCodError()+"-"+e.getMessage());
		}
		
		try {
			if(validarCadena(apellido)) {
				System.out.println("Apellido Correcto");
			}
		}catch(ValidacionException e) {
			System.out.println("Error en la validación del apellido");
		}
		try {
			if(validarEntero(edad)) {
				System.out.println("Edad Correcto");
			}
		}catch(ValidacionException e) {
			System.out.println("Error en la validación de edad");
		}
		
	}
		private static boolean validarCadena(String cadena) throws ValidacionException {
			if(cadena.length()<3 || cadena.length()>15)
				throw new ValidacionException("Error validacion cadena", 285);
			else
			return true;
		}
		private static boolean validarEntero(int entero) throws ValidacionException {
			if(entero<0 || entero>150)
				throw new ValidacionException();
			else
			return true;
		}
}
