package gob.ine.cognos.javadoc.Calculadora;

/**
 * Clase que sirve para realizar calculos
 * @author Angela Cáceres
 *
 */
public class Calculara {
	/**
	 * Atributo para sumar
	 */
	private int numero1;
	private int numero2;
	
	public Calculara()  {
		
	}
	/**
	 * @param numero1  Parametro 1 del constructor
	 * @param numero2  Parametro 2 del constructor
	 */
	public Calculara(int numero1, int numero2) {
		this.numero1 = numero1;
		this.numero2 = numero2;
	}
	public int getNumero1() {
		return numero1;
	}
	public void setNumero1(int numero1) {
		this.numero1 = numero1;
	}
	public int getNumero2() {
		return numero2;
	}
	public void setNumero2(int numero2) {
		this.numero2 = numero2;
	}
	
	/**
	 * @param numeroSumando
	 * @param etiqueta
	 * @return Retorna valor entero que es la suma del sumando + otros numeros declarados
	 */
	public int calcular(int numeroSumando, String etiqueta) {
		System.out.println (etiqueta + (numero1 + numero2 + numeroSumando));
		return numero1 + numero2 + numeroSumando;
	}
}
