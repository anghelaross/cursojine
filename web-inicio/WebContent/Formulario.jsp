<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Mi Formulario</title>
</head>
<body>
	<form action="Contenido.jsp" method="post">
		<table>
			<tr>
				<td>
					Nombre:
				</td>
				<td>
					<input type="text" name="nombre">
				</td>
			</tr>
			<tr>
				<td>
					Apellido:
				</td>
				<td>
					<input type="text" name="apellido">
				</td>
			</tr>
			<tr>
				<td>
					Profesión:
				</td>
				<td>
					<select name="profesion">
						<option value="Ingeniero de Sistemas" />Ingeniero de Sistemas</option>
						<option value="Informático" />Informático</option>
						<option value="Profesor" />Profesor</option>
					</select>
				</td>
			</tr>
			<tr>
				<td colspan="2">
					<input type="submit" value="Mostrar" name="mostrar">
				</td>
			</tr>
		</table>
	
	</form>
</body>
</html>