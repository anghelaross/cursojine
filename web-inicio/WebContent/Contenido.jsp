
<%@page import="java.util.Date"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>
</head>
<body>
	<table>
			<tr>
				<td colspan="2">
					<b>Datos Usuario <%= new Date() %></b>
				</td>
			</tr>
			<tr>
				<td>
					Nombre:
				</td>
				<td>
					<%= request.getParameter("nombre") %>
				</td>
			</tr>
			<tr>
				<td>
					Apellido:
				</td>
				<td>
					<%= request.getParameter("apellido") %>
				</td>
			</tr>
			<tr>
				<td>
					Profesión:
				</td>
				<td>
					<%= request.getParameter("profesion") %>
				</td>
			</tr>
			
		</table>
</body>
</html>