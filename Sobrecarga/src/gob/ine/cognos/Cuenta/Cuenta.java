package gob.ine.cognos.Cuenta;

public class Cuenta {
	private String nombret;
	private int nroc;
public Cuenta() {
		
	}
	public Cuenta(String nombret, int nroc) {
		
		this.nombret = nombret;
		this.nroc = nroc;
	}
	
	public String getNombret() {
		return nombret;
	}
	public void setNombret(String nombret) {
		this.nombret = nombret;
	}
	public int getNroc() {
		return nroc;
	}
	public void setNroc(int nroc) {
		this.nroc = nroc;
	}
	public void modificar(String nombret) {
		this.nombret=nombret;
	}
	public void modificar(int nroc) {
		this.nroc=nroc;
	}
	public void modificar(String nombret, int nroc) {
		this.nombret=nombret;
		this.nroc=nroc;
	}
}
