package gob.ine.cognos.Cuenta;

public class Main {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Cuenta cuenta=new Cuenta();
		cuenta.setNombret("Angela");
		cuenta.setNroc(89456123);
		
		System.out.println("Titular= "+cuenta.getNombret()+" NRO. CUENTA: "+cuenta.getNroc());
		
		cuenta.modificar("Juan Carlos");
		System.out.println("Titular= "+cuenta.getNombret()+" NRO. CUENTA: "+cuenta.getNroc());
		cuenta.modificar(88888888);
		System.out.println("Titular= "+cuenta.getNombret()+" NRO. CUENTA: "+cuenta.getNroc());
		cuenta.modificar("Angela", 12121212);
		System.out.println("Titular= "+cuenta.getNombret()+" NRO. CUENTA: "+cuenta.getNroc());
	}

}
