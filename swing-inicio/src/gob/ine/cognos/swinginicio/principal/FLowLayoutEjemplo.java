package gob.ine.cognos.swinginicio.principal;

import java.awt.Color;
import java.awt.FlowLayout;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;

public class FLowLayoutEjemplo extends JFrame {
	private JButton jbBoton1;
	private JButton jbBoton2;
	private JButton jbBoton3;
	public FLowLayoutEjemplo() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setSize(100, 100);
		//setResizable(false);
		JPanel panelPrincipal=(JPanel) getContentPane();
		panelPrincipal.setBackground(Color.GREEN);
		panelPrincipal.setLayout(new FlowLayout(FlowLayout.RIGHT));
		jbBoton1 = new JButton("BOTON 1");
		jbBoton2 = new JButton("BOTON 2");
		jbBoton3 = new JButton("BOTON 3");
		panelPrincipal.add(jbBoton1);
		panelPrincipal.add(jbBoton2);
		panelPrincipal.add(jbBoton3);
		pack();
	}
	public static void main(String[] args)
	{
		FLowLayoutEjemplo ejemplo=new FLowLayoutEjemplo();
	    ejemplo.setVisible(true);
	}
}
