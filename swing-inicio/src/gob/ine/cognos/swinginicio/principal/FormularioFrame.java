package gob.ine.cognos.swinginicio.principal;

import java.awt.Color;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JTextField;

public class FormularioFrame extends JFrame {
	private JLabel jlVacio;
	private JLabel jlLogin;
	private JLabel jlPassword;
	private JTextField jtxLogin;
	private JPasswordField jpPassword;
	private JButton jbIngresar;
	
	
	public FormularioFrame() {
		super.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		super.setSize(100, 100);
		//setResizable(false);
		JPanel panelPrincipal=(JPanel) getContentPane();
		panelPrincipal.setBackground(Color.GREEN);
		panelPrincipal.setLayout(new GridLayout(3,2));
		jlVacio = new JLabel("");
		jlLogin = new JLabel("Login: ");
		jlPassword = new JLabel("Password: ");
		jtxLogin = new JTextField(15);
		
		jpPassword = new JPasswordField(15);
		jbIngresar = new JButton("INGRESAR");
		
		jlLogin.setBackground(Color.CYAN);
		jlLogin.setText("Usuario: ");
		//jtxLogin.setText("ANGELA");
		
		jbIngresar.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				String login =jtxLogin.getText();
				String password = new String (jpPassword.getPassword());
				System.out.println(password.length() + "-" + password);
				if (login.equalsIgnoreCase("ANGELA") && password.equalsIgnoreCase("12345678")) {
					//JOptionPane.showMessageDialog(null, "Hola " + login);
					ContenidoFrame contenidoFrame = new ContenidoFrame();
					contenidoFrame.setVisible(true);
				}
				else
				JOptionPane.showMessageDialog(null, "NO es correcto");
				
			}
		});
		
		panelPrincipal.add(jlLogin);
		panelPrincipal.add(jtxLogin);
		panelPrincipal.add(jlPassword);
		panelPrincipal.add(jpPassword);
		panelPrincipal.add(jlVacio);
		panelPrincipal.add(jbIngresar);
		
		pack();
	}
	public static void main(String[] args)
	{
		FormularioFrame ejemplo=new FormularioFrame();
	    ejemplo.setVisible(true);
	}
}

