package gob.ine.cognos.swinginicio.principal;

import java.awt.BorderLayout;
import java.awt.Color;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;

public class GridLayoutEjemplo extends JFrame {
	private JButton jbBoton1;
	private JButton jbBoton2;
	private JButton jbBoton3;
	private JButton jbBoton4;
	private JButton jbBoton5;
	
	public GridLayoutEjemplo() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);		
		//JPanel panelPrincipal=(JPanel) getContentPane();
		//PanelPersonal panelPersonal=new PanelPersonal();
		//setContentPane(panelPersonal);
		JPanel panelPrincipal=(JPanel) getContentPane();
		panelPrincipal.setLayout(new BorderLayout());
		jbBoton1 = new JButton("BOTON 1");
		jbBoton2 = new JButton("BOTON 2");
		jbBoton3 = new JButton("BOTON 3");
		jbBoton4 = new JButton("BOTON 4");
		jbBoton5 = new JButton("BOTON 5");
		
		panelPrincipal.add(jbBoton1, BorderLayout.CENTER);
		panelPrincipal.add(jbBoton2, BorderLayout.WEST);
		panelPrincipal.add(jbBoton3, BorderLayout.NORTH);
		panelPrincipal.add(jbBoton4, BorderLayout.SOUTH);
		panelPrincipal.add(jbBoton5, BorderLayout.EAST);
		pack();
	}
	public static void main(String[] args)
	{
		GridLayoutEjemplo ejemplo=new GridLayoutEjemplo();
	    ejemplo.setVisible(true);
	}
}
