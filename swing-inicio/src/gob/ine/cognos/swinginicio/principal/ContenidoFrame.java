package gob.ine.cognos.swinginicio.principal;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.JPanel;

public class ContenidoFrame extends JFrame {
	JLabel jlbSaludo;
	JList jlsLista;
	JButton jbtSeleccionar;
	
	public ContenidoFrame() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		JPanel panelPrincipal = (JPanel) getContentPane();
		panelPrincipal.setLayout(new BoxLayout(panelPrincipal, BoxLayout.Y_AXIS));
		jlbSaludo =new JLabel("Lista Personas");
		String[] data = {"Sergio","Roberto","Angela","Juan Carlos"};
		jlsLista=new JList(data);
		jbtSeleccionar = new JButton("Mostrar Seleccionado");
		jbtSeleccionar.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent arg0) {
				String seleccionado = (String) jlsLista.getSelectedValue();
				JOptionPane.showMessageDialog(null, "El seleccionado es: " + seleccionado);
			}
		});
		panelPrincipal.add(jlbSaludo);
		panelPrincipal.add(jlsLista);
		panelPrincipal.add(jbtSeleccionar);
		//getContentPane().add(jlbSaludo);
		pack();
	}
	public static void main(String[] args) {
		ContenidoFrame contenidoFrame = new ContenidoFrame();
		contenidoFrame.setVisible(true);
	}
}
