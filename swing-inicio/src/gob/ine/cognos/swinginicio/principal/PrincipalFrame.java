package gob.ine.cognos.swinginicio.principal;
import javax.swing.JButton;
import javax.swing.JFrame;
public class PrincipalFrame extends JFrame{
	
	private JButton jbBoton1;
	private JButton jbBoton2;
	
	public PrincipalFrame() {
		jbBoton1=new JButton("CLICK");
		jbBoton2=new JButton("CLICK-2");
		getContentPane().add(jbBoton1);
		getContentPane().add(jbBoton2);
		pack();
	}

	public static void main(String[] args) {
		PrincipalFrame principalFrame=new PrincipalFrame();
		principalFrame.setVisible(true);

	}

}
