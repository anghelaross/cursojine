package gob.ine.cognos.swinginicio.principal;


import java.awt.BorderLayout;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;

public class BorderLayoutEjemplo extends JFrame{
	private JButton jbBoton1;
	private JButton jbBoton2;
	private JButton jbBoton3;
	private JButton jbBoton4;
	
	public BorderLayoutEjemplo() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);	
		
		PanelPersonal panelPersonal=new PanelPersonal();
		setContentPane(panelPersonal);
		JPanel panelPrincipal=(JPanel) getContentPane();
		panelPrincipal.setLayout(new BorderLayout());
		jbBoton1 = new JButton("BOTON 1");
		jbBoton2 = new JButton("BOTON 2");
		jbBoton3 = new JButton("BOTON 3");
		jbBoton4 = new JButton("BOTON 4");
		panelPrincipal.add(jbBoton1);
		panelPrincipal.add(jbBoton2);
		panelPrincipal.add(jbBoton3);
		panelPrincipal.add(jbBoton4);
		pack();
	}
	public static void main(String[] args)
	{
		BorderLayoutEjemplo ejemplo=new BorderLayoutEjemplo();
	    ejemplo.setVisible(true);
	}
}
