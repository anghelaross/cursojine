<%@page import="gob.ine.cognos.jsfjdbc.dao.PersonaDAO"%>
<%@page import="gob.ine.cognos.jsfjdbc.model.Persona"%>
<%@page import="java.util.List"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>
</head>
<body>
	<%
		List<Persona> personas=PersonaDAO.listar();
	request.setAttribute("personas", personas);
	%>
	<table>
		<tr>
			<td>Id</td>
			<td>Nombre</td>
			<td>Email</td>
			<td>Genero</td>
			<td>Edad</td>
		</tr>
		<c:forEach items="${personas}" var="p">
		<tr>
			<td>${p.getId()}</td>
			<td>${p.getNombre()}</td>
			<td>${p.getEmail()}</td>
			<td>${p.getGenero()}</td>
			<td>${p.getEdad()}</td>
		</tr>
		</c:forEach>
	</table>
</body>
</html>