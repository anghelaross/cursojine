package gob.ine.cognos.jsfjdbc.model;

import java.io.Serializable;

public class Persona implements Serializable{
	private int id;
	private String nombre;
	private String email;
	private String genero;
	private int edad;
	
	public Persona() {
		
	}
	
	public Persona(int id, String nombre, String email, String genero, int edad) {
		this.id = id;
		this.nombre = nombre;
		this.email = email;
		this.genero = genero;
		this.edad = edad;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getGenero() {
		return genero;
	}
	public void setGenero(String genero) {
		this.genero = genero;
	}
	public int getEdad() {
		return edad;
	}
	public void setEdad(int edad) {
		this.edad = edad;
	}
	
	
	
}
