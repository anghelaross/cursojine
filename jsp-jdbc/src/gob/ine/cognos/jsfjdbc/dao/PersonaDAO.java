package gob.ine.cognos.jsfjdbc.dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import gob.ine.cognos.jsfjdbc.model.Persona;

public class PersonaDAO {
	
	private static Connection getConnection() {
		Connection con = null;
		try {
			Class.forName("org.postgresql.Driver");
			con = DriverManager.getConnection("jdbc:postgresql://localhost:5432/postgres?currentSchema=curso_java","postgres","patito");
			
		} catch (ClassNotFoundException e) {
			System.out.println("Error al cargar DRiver " + e.getMessage());
		} catch (SQLException e) {
			System.out.println("NO se pudo abrir la conección " + e.getMessage());
		}
		
		return con;
	}
	
	public static List<Persona> listar() {
		List<Persona> personas = new ArrayList<Persona>();
		Connection con =getConnection();
		ResultSet rs = null;
		PreparedStatement ps= null;
		
		if(con != null) {
			String sql = "select * from persona";
			try {
				ps = con.prepareStatement(sql);
				rs=ps.executeQuery();
				while(rs.next()) {
					Persona persona = new Persona();
					persona.setId(rs.getInt("id"));
					persona.setNombre(rs.getString("nombre"));
					persona.setEmail(rs.getString("email"));
					persona.setGenero(rs.getString("genero"));
					persona.setEdad(rs.getInt("edad"));
					personas.add(persona);
				}
			} catch (Exception e) {
				System.out.println("La consulta no puede ser ejecutada " + e.getMessage());
			} finally {
				if(rs!=null) {
					try {
						rs.close();
					} catch (Exception e2) {
						System.out.println("Error al cerrar RS " + e2.getMessage());
					}
				}
				if(ps!=null) {
					try {
						ps.close();
					} catch (Exception e2) {
						System.out.println("Error al cerrar PS " + e2.getMessage());
					}
				}
				if(con!=null)
					try {
						con.close();
					} catch (SQLException e) {
						System.out.println("Error al cerrar");
						//e.printStackTrace();
					}
			}
			
		}
		else {
			System.out.println("NO se pudo abir la conecciòn ");
		}
		return personas;
	}
	
	
	
}
