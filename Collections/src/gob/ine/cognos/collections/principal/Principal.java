package gob.ine.cognos.collections.principal;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;

//,"dos","tres","cuatro"
public class Principal {

	public static void main(String[] args) {
		List cadenas = new ArrayList();
		cadenas.add("uno");
		cadenas.add(1);
		cadenas.add(new Persona("Pedro", "Perez"));

		for (int i = 0; i < cadenas.size(); i++) {
			System.out.println(cadenas.get(i));
			if (cadenas.get(i) instanceof Persona)
				System.out.println("Imprimiendo apellido - " + ((Persona) cadenas.get(i)).getApellido());
		}
		List<Persona> personas = new ArrayList<Persona>();
		personas.add(new Persona("Pedro", "Perez"));
		personas.add(new Persona("Angela", "Perez"));
		personas.add(new Persona("Juan Carlos", "Perez"));
		personas.add(new Persona("Roberto", "Perez"));
		Persona personaSergio = new Persona();
		personaSergio.setNombre("Sergio");
		personaSergio.setApellido("Perez");
		personaSergio.setEdad(74);
		personas.add(2, personaSergio); // AdiciONA ADD
		personas.set(2, personaSergio); // REemplaZA SET

		for (int i = 0; i < personas.size(); i++) {
			Persona personaItem = personas.get(i);
			// System.out.println(cadenas.get(i));
			System.out.println("Imprimiendo: " + personaItem);
			System.out.println("NOMBRE ->" + personaItem.getNombre());
		}
		System.out.println();

		if (personas.contains(personaSergio))
			System.out.println("Sergio está");
		personas.remove(2);

		for (Persona personaItem : personas) {
			System.out.println(personaItem);
			System.out.println("EDAD -> " + personaItem.getEdad());
		}
		if (personas.contains(personaSergio))
			System.out.println("Sergio está");
		else
			System.out.println("Sergio no está");
		
		List<String> cadenasLinked = new LinkedList<String>();
		//Queue<String> cadenasLinked = new <String>()
		cadenasLinked.add("Prueba1");
		cadenasLinked.add("Prueba2");
		cadenasLinked.add("Prueba3");
		for (String cadena : cadenasLinked) {
			System.out.println(cadena);
		}
		
	}

}
